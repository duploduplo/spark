#include "spark.h"

#include "lauxlib.h"
#include "lua.h"
#include "lua_engine.h"
#include "lualib.h"
#include <glib.h>
#include <string.h>

struct LuaEngine {
	lua_State *L;
	GList *errors;
	LuaEngineHook new_object_hook;
	LuaEngineHook free_object_hook;

	// GHashTable[int]->GHashTable[char*]->Property*
	GHashTable *object_properties;
};

typedef struct CFuncUserdata {
	int argc;
	int *argtv;
	char *name;
	Func f;
} CFuncUserdata;

// TODO: remove this and the definition in script.c, entity.c
static void
free_hash_table(gpointer ptr)
{
	g_hash_table_destroy(ptr);
}

static void
push_func(lua_State *L, const char *name, Func f, int argc, int *argtv);

static void
push_value(lua_State *L, Value *v);

static Value
get_value(lua_State *L, int index);

static int
spark_type_to_lua(int t)
{
	switch (t) {
	case SCRIPT_TYPE_NULL:
		return LUA_TNIL;

	case SCRIPT_TYPE_INT:
	case SCRIPT_TYPE_FLOAT:
		return LUA_TNUMBER;

	case SCRIPT_TYPE_STRING:
		return LUA_TSTRING;

	case SCRIPT_TYPE_BOOL:
		return LUA_TBOOLEAN;

	case SCRIPT_TYPE_OBJECT:
		return LUA_TTABLE;

	case SCRIPT_TYPE_FUNCTION:
		return LUA_TFUNCTION;

	default:
		return -1;
	}
}

static LuaEngine*
get_engine_ptr(lua_State *L)
{
	lua_getglobal(L, "__engine");
	LuaEngine *ptr = lua_touserdata(L, lua_gettop(L));
	lua_pop(L, 1);
	return ptr;
}

static bool
type_eq(int lua_type, int spark_type) {
	return lua_type == spark_type_to_lua(spark_type) || spark_type == SCRIPT_TYPE_VARARG;
}

static Value
object__tostring(int argc, Value *argv)
{
	TypeInfo *ti = spark_script_type_info(argv[0].val.object.type);
	if (!ti) {
		spark_log(
			LOG_FATAL,
			"unknown object type %d", argv[0].val.object.type
		);
	}

	char *s;
	if (ti->str)
		s = ti->str(&argv[0].val.object);
	else
		s = g_strdup_printf("%s", ti->name);

	Value v = {
		.type = SCRIPT_TYPE_STRING,
		.val.string = s
	};
	return v;
}

static int
object__gc(lua_State *L)
{
	Object *o = lua_touserdata(L, -1);
	TypeInfo *ti = spark_script_type_info(o->type);
	if (ti && ti->dtor)
		ti->dtor(o);

	LuaEngine *le = get_engine_ptr(L);
	g_hash_table_remove(le->object_properties, GINT_TO_POINTER(o->ref));

	return 0;
}

static int
object__index(lua_State *L)
{
	// retrieve the Object associated with the table
	lua_pushstring(L, "__data");
	lua_rawget(L, 1);
	Object *o = lua_touserdata(L, -1);
	lua_pop(L, 1);

	if (!o) {
		lua_rawget(L, 1);
		return 1;
	}

	// get the map of properties for given object
	const char *name = lua_tostring(L, -1);
	LuaEngine *le = get_engine_ptr(L);
	GHashTable *properties = g_hash_table_lookup(
		le->object_properties, GINT_TO_POINTER(o->ref)
	);

	// retrieve the property definition for given name
	Property *prop = NULL;
	if (properties) {
		prop = g_hash_table_lookup(properties, name);
	}
	if (prop) {
		// push the value returned by getter
		Value prop_val = prop->getter(o);
		push_value(L, &prop_val);
	}
	else {
		// perform a raw get
		lua_rawget(L, -2);
	}

	return 1;
}

static int
object__newindex(lua_State *L)
{
	// retrieve the Object associated with the table
	lua_pushstring(L, "__data");
	lua_rawget(L, 1);
	Object *o = lua_touserdata(L, -1);
	lua_pop(L, 1);

	if (!o) {
		lua_rawset(L, 1);
		return 0;
	}

	// get the map of properties for given object
	LuaEngine *le = get_engine_ptr(L);
	GHashTable *properties = g_hash_table_lookup(
		le->object_properties, GINT_TO_POINTER(o->ref)
	);

	// retrieve the property definition for given name
	const char *name = lua_tostring(L, -2);
	Property *prop = NULL;
	if (properties) {
		prop = g_hash_table_lookup(properties, name);
	}

	if (prop) {
		if (!prop->setter) {
			luaL_error(
				L,
				"property %s is read-only",
				name
			);
			return 0;
		}
		// attempt to convert the lua value to spark value
		Value val = get_value(L, -1);

		// check types
		if (prop->type != val.type) {
			luaL_error(L, "unsupported type for property %s", name);
			return 0;
		}

		// call the setter function
		Value err_val = prop->setter(o, &val);
		if (err_val.type == SCRIPT_TYPE_ERROR) {
			luaL_error(
				L,
				"failed to set property %s: %s",
				name,
				err_val.val.string
			);
		}
	}
	else {
		// perform a raw set
		lua_rawset(L, -3);
	}

	return 0;
}

static void
init_object(lua_State *L, Object *o)
{
	// push the object's on top of the stack
	lua_rawgeti(L, LUA_REGISTRYINDEX, o->ref);
	int t = lua_gettop(L);

	// push the key at which data will be stored
	lua_pushstring(L, "__data");

	// create a clone of the object as Lua userdata
	// TODO: implement the freeing of this
	Object *o_copy = lua_newuserdata(L, sizeof(Object));
	memcpy(o_copy, o, sizeof(Object));
	int u = lua_gettop(L);

	// create userdata metatable
	lua_createtable(L, 0, 1);

	// push the __gc() metamethod
	lua_pushstring(L, "__gc");
	lua_pushcfunction(L, object__gc);
	lua_settable(L, -3);

	lua_setmetatable(L, u);

	// store the userdata
	lua_settable(L, t);

	// create the metatable
	lua_createtable(L, 0, 1);

	// push the __tostring() metamethod
	lua_pushstring(L, "__tostring");
	int argtv[] = { SCRIPT_TYPE_OBJECT };
	push_func(L, "__tostring", object__tostring, 1, argtv);
	lua_settable(L, -3);

	// push the __index() metamethod
	lua_pushstring(L, "__index");
	lua_pushcfunction(L, object__index);
	lua_settable(L, -3);

	// push the __newindex() metamethod
	lua_pushstring(L, "__newindex");
	lua_pushcfunction(L, object__newindex);
	lua_settable(L, -3);

	// set object's metatable
	lua_setmetatable(L, t);

	// call the constructor
	TypeInfo *ti = spark_script_type_info(o->type);
	if (ti && ti->ctor)
		ti->ctor(o);

	// call the new object hook
	LuaEngine *le = get_engine_ptr(L);
	if (le && le->new_object_hook)
		le->new_object_hook(le, o);
}

static void
push_value(lua_State *L, Value *v)
{
	switch (v->type) {
	case SCRIPT_TYPE_NULL:
		lua_pushnil(L);
		break;

	case SCRIPT_TYPE_BOOL:
		lua_pushboolean(L, v->val.integer);
		break;

	case SCRIPT_TYPE_FLOAT:
		lua_pushnumber(L, v->val.real);
		break;

	case SCRIPT_TYPE_INT:
		lua_pushinteger(L, v->val.integer);
		break;

	case SCRIPT_TYPE_STRING:
		lua_pushstring(L, v->val.string);
		break;

	case SCRIPT_TYPE_OBJECT:
		// create a new table and a reference for it
		lua_newtable(L);
		v->val.object.ref = luaL_ref(L, LUA_REGISTRYINDEX);

		// initialize object
		init_object(L, &v->val.object);
		break;

	case SCRIPT_TYPE_FUNCTION:
		lua_rawgeti(L, LUA_REGISTRYINDEX, v->val.funcref);
		break;

	default:
		spark_log(LOG_FATAL, "unknown value type (%d)", v->type);
	}
}

static Value
get_value(lua_State *L, int index)
{
	Value v;
	index = lua_absindex(L, index);

	int t = lua_type(L, index);
	switch (t) {
	case LUA_TNIL:
		v.type = SCRIPT_TYPE_NULL;
		break;

	case LUA_TBOOLEAN:
		v.type = SCRIPT_TYPE_BOOL;
		v.val.integer = lua_toboolean(L, index);
		break;

	case LUA_TNUMBER:
		v.val.real = lua_tonumber(L, index);
		v.type = SCRIPT_TYPE_FLOAT;
		if (v.val.real == (int)v.val.real) {
			v.type = SCRIPT_TYPE_INT;
			v.val.integer = (int)v.val.real;
		}
		break;

	case LUA_TSTRING:
		v.type = SCRIPT_TYPE_STRING;
		v.val.string = g_strdup(lua_tostring(L, index));
		break;

	case LUA_TFUNCTION:
		v.type = SCRIPT_TYPE_FUNCTION;
		v.val.funcref = luaL_ref(L, LUA_REGISTRYINDEX);
		break;

	case LUA_TTABLE:
		v.type = SCRIPT_TYPE_OBJECT;
		lua_pushstring(L, "__data");
		lua_gettable(L, index);
		if (lua_type(L, -1) == LUA_TUSERDATA) {
			v.val.object = *(Object*)lua_touserdata(L, -1);
			lua_pop(L, 1);
			break;
		}
		// otherwise falls-through

	default:
		spark_log(
			LOG_FATAL,
			"unsupported value type %d (%s)",
			t,
			lua_typename(L, t)
		);
	}

	return v;
}

static int
cfunc_userdata__call(lua_State *L)
{
	CFuncUserdata *fu = lua_touserdata(L, 1);

	int argc = lua_gettop(L) - 1;

	// check arguments number
	if (fu->argc > 0 &&
	    fu->argtv[fu->argc - 1] == SCRIPT_TYPE_VARARG) {
		if (argc < fu->argc - 1) {
			luaL_error(
				L,
				"bad call to variadic %s: expected at least %d arguments (%d given)",
				fu->name,
				fu->argc - 1,
				argc
			);
		}
	}
	else if (argc != fu->argc) {
		luaL_error(
			L,
			"bad call to %s: expected %d arguments (%d given)",
			fu->name,
			fu->argc,
			argc
		);
		return 0;
	}

	// check argument types
	for (int i = 0; i < fu->argc; i++) {
		int t = lua_type(L, i + 2);
		if (!type_eq(t, fu->argtv[i])) {
			luaL_error(
				L,
				"bad call to %s: mismatching types for argument %d: expected %s, got %s",
				fu->name,
				i,
				lua_typename(L, spark_type_to_lua(fu->argtv[i])),
				lua_typename(L, t)
			);
		}
	}

	// populate the array of values
	Value argv[argc];
	for (int i = 0; i < argc; i++) {
		argv[i] = get_value(L, i + 2);
	}

	// call the function
	Value retval = fu->f(argc, argv);

	// free arguments
	for (int i = 0; i < argc; i++) {
		if (argv[i].type == SCRIPT_TYPE_STRING)
			g_free(argv[i].val.string);
	}

	// report an error if the function call failed, otherwise push the
	// return value
	if (retval.type == SCRIPT_TYPE_ERROR) {
		luaL_error(
			L,
			"bad call to %s: %s",
			fu->name,
			retval.val.string ?
			retval.val.string :
			"unknown error"
		);
	}
	else if (retval.type != SCRIPT_TYPE_NULL) {
		push_value(L, &retval);
	}

	if (retval.type == SCRIPT_TYPE_STRING)
		g_free(retval.val.string);

	return retval.type != SCRIPT_TYPE_NULL && retval.type != SCRIPT_TYPE_ERROR;
}

static void
fetch_error(LuaEngine *le)
{
	le->errors = g_list_prepend(
		le->errors,
		g_strdup(lua_tostring(le->L, -1))
	);
	lua_pop(le->L, 1);
}

static void
push_error(LuaEngine *le, char *err)
{
	le->errors = g_list_prepend(le->errors, err);
}

static void
push_func(lua_State *L, const char *name, Func f, int argc, int *argtv)
{
	// create new userdata object and store its index
	CFuncUserdata *fu = lua_newuserdata(L, sizeof(CFuncUserdata));
	int userdata_idx = lua_gettop(L);

	// setup the userdata object
	// TODO: implement proper clean-up of argtv and name attributes
	fu->argc = argc;
	fu->argtv = g_memdup(argtv, sizeof(int) * argc);
	fu->f = f;
	fu->name = g_strdup(name);

	// create an empty
	lua_createtable(L, 0, 1);
	int metatable_idx = lua_gettop(L);

	// set the __call key to cfunc_userdata__call()
	lua_pushstring(L, "__call");
	lua_pushcfunction(L, cfunc_userdata__call);
	lua_settable(L, metatable_idx);

	// set the created table as userdata object's metatable
	lua_setmetatable(L, userdata_idx);
}

LuaEngine*
lua_engine_new()
{
	LuaEngine *le = g_new0(LuaEngine, 1);

	le->L = luaL_newstate();
	luaL_openlibs(le->L);

	le->object_properties = g_hash_table_new_full(
		NULL, NULL, NULL, free_hash_table
	);

	// store the pointer to this engine instance as __engine global
	lua_pushlightuserdata(le->L, le);
	lua_setglobal(le->L, "__engine");
	return le;
}

int
lua_engine_run(LuaEngine *le, const char *name, const char *code)
{
	int error = luaL_loadbuffer(le->L, code, strlen(code), name);
	if (error) {
		fetch_error(le);
		return error;
	}

	error = lua_pcall(le->L, 0, 0, 0);
	if (error) {
		fetch_error(le);
		return error;
	}

	return error;
}

char*
lua_engine_get_error(LuaEngine *le)
{
	char *err = le->errors->data;
	le->errors = g_list_remove_link(le->errors, le->errors);
	return err;
}

void
lua_engine_free(LuaEngine *le)
{
	lua_close(le->L);

	g_hash_table_destroy(le->object_properties);

	g_free(le);
}

void
lua_engine_reg_func(
	LuaEngine *le,
	const char *name,
	Func f,
	int argc,
	int *argtv
) {
	push_func(le->L, name, f, argc, argtv);
	lua_setglobal(le->L, name);
}

int
lua_engine_reg_lib_func(
	LuaEngine *le,
	const char *libname,
	const char *name,
	Func f,
	int argc,
	int *argtv
) {
	// check whether the library table already exists and create one on
	// need
	lua_getglobal(le->L, libname);
	int t = lua_type(le->L, -1);

	if (t == LUA_TNIL) {
		lua_createtable(le->L, 0, 1);
		lua_setglobal(le->L, libname);
		lua_getglobal(le->L, libname);
	}
	else if (t != LUA_TTABLE) {
		char *err = g_strdup_printf(
			"cannot store function '%s' in '%s': bad type (%s)",
			name,
			libname,
			lua_typename(le->L, t)
		);
		push_error(le, err);
		return 1;
	}

	// store the function in table
	int table_index = lua_gettop(le->L);
	lua_pushstring(le->L, name);
	push_func(le->L, name, f, argc, argtv);
	lua_settable(le->L, table_index);

	return 0;
}

static Value
call(LuaEngine *le, int argc, Value *argv)
{
	int t = lua_type(le->L, -1);

	Value retval = {
		.type = SCRIPT_TYPE_NULL,
	};

	if (t != LUA_TFUNCTION) {
		retval.type = SCRIPT_TYPE_ERROR;
		retval.val.string = g_strdup_printf(
			"%s is not a callable",
			lua_typename(le->L, t)
		);
		return retval;
	}

	for (int i = 0; i < argc; i++) {
		push_value(le->L, argv + i);
	}

	lua_call(le->L, argc, 1);
	retval = get_value(le->L, -1);

	return retval;
}

Value
lua_engine_call(LuaEngine *le, const char *func, int argc, Value *argv)
{
	lua_getglobal(le->L, func);
	Value v = call(le, argc, argv);
	lua_pop(le->L, 1);
	return v;
}

Value
lua_engine_call_by_ref(LuaEngine *le, int funcref, int argc, Value *argv)
{
	lua_rawgeti(le->L, LUA_REGISTRYINDEX, funcref);
	Value v = call(le, argc, argv);
	lua_pop(le->L, 1);
	return v;
}

bool
lua_engine_has_func(LuaEngine *le, const char *func)
{
	lua_getglobal(le->L, func);
	int t = lua_type(le->L, -1);
	lua_pop(le->L, 1);

	return t == LUA_TFUNCTION;
}

void
lua_engine_attach_method(
	LuaEngine *le,
	Object *o,
	const char *name,
	Func f,
	int argc,
	int *argtv
) {
	lua_rawgeti(le->L, LUA_REGISTRYINDEX, o->ref);
	int t = lua_gettop(le->L);

	lua_pushstring(le->L, name);
	push_func(le->L, name, f, argc, argtv);
	lua_settable(le->L, t);
	lua_pop(le->L, 1);
}

void
lua_engine_set_new_object_hook(LuaEngine *le, LuaEngineHook f)
{
	le->new_object_hook = f;
}

void
lua_engine_set_free_object_hook(LuaEngine *le, LuaEngineHook f)
{
	le->free_object_hook = f;
}

void
lua_engine_set_attr(LuaEngine *le, int ref, const char *name, Value *attr)
{
	lua_rawgeti(le->L, LUA_REGISTRYINDEX, ref);
	int t = lua_gettop(le->L);

	lua_pushstring(le->L, name);

	push_value(le->L, attr);

	lua_settable(le->L, t);
	lua_pop(le->L, 1);
}

Value
lua_engine_get_attr(LuaEngine *le, int ref, const char *name)
{
	lua_rawgeti(le->L, LUA_REGISTRYINDEX, ref);
	int t = lua_gettop(le->L);

	lua_pushstring(le->L, name);

	lua_gettable(le->L, t);
	Value v = get_value(le->L, -1);

	lua_pop(le->L, 1);
	return v;
}

void
lua_engine_define_property(
	LuaEngine *le,
	int ref,
	const char *name,
	Property *prop
) {
	GHashTable *props = g_hash_table_lookup(
		le->object_properties, GINT_TO_POINTER(ref)
	);
	if (!props) {
		props = g_hash_table_new_full(
			g_str_hash, g_str_equal, g_free, g_free
		);
		g_hash_table_insert(
			le->object_properties, GUINT_TO_POINTER(ref), props
		);
	}

	g_hash_table_insert(props, g_strdup(name), g_memdup(prop, sizeof(Property)));
}
