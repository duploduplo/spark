#include "spark.h"

#include "containers/pool.h"
#include "internals.h"
#include <glib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef SPARK_LOG_LEVEL
#define SPARK_LOG_LEVEL LOG_ERROR
#endif

#define EVENTS_BUFFER_SIZE 100

#define ENTITIES_POOL_SIZE 1000

#define assert_entity_is_valid(__entity) g_assert(__entity != 0);

#if defined(HAS_LUA)
extern void
script_init();

extern void
script_exit();

extern bool
script_update();
#endif

static void
read_config(const char *config_file)
{
	GError *err = NULL;

	bool loaded = g_key_file_load_from_file(
		game.cfg,
		config_file,
		G_KEY_FILE_NONE,
		&err
	);

	if (!loaded) {
		spark_log(
			LOG_FATAL,
			"failed to load config file '%s': %s",
			config_file,
			err->message
		);
	}
}

void
destroy_entity_components(EntityInfo *einfo)
{
	GHashTableIter iter;
	gpointer key, value;

	g_hash_table_iter_init(&iter, einfo->components);
	while (g_hash_table_iter_next(&iter, &key, &value)) {
		struct System *sys = g_hash_table_lookup(game.systems, key);
		if (sys->destroy_component) {
			sys->destroy_component(sys, einfo->entity);
			spark_log(
				LOG_DEBUG,
				"destroyed component '%s' of entity %d",
				sys->name,
				einfo->entity
			);
		}
	}
	g_hash_table_destroy(einfo->components);
}

EXPORT void
spark_init(const char *config_file)
{
	game.systems = g_hash_table_new(NULL, NULL);
	game.sys_names_map = g_hash_table_new(g_str_hash, g_str_equal);
	game.subscriptions = g_hash_table_new(NULL, NULL);
	game.reg_order = g_array_new(false, false, sizeof(ComponentType));
	game.event_queue = pool_new(sizeof(struct Event), 10, 10);
	game.log_level = SPARK_LOG_LEVEL;
	game.hooks.entity_created = g_array_new(false, false, sizeof(struct HookInfo));
	game.hooks.entity_destroyed = g_array_new(false, false, sizeof(struct HookInfo));

	game.entities = pool_new(
		sizeof(struct EntityInfo),
		ENTITIES_POOL_SIZE,
		ENTITIES_POOL_SIZE / 10
	);

#if defined(HAS_LUA)
	script_init();
#endif

	// read configuration
	game.cfg = g_key_file_new();
	if (config_file)
		read_config(config_file);
}

EXPORT void
spark_exit()
{
	// destroy entities
	int size = pool_size(game.entities);
	for (int i = 0; i < size; i++) {
		struct EntityInfo *einfo = pool_get(game.entities, i);
		if (einfo)
			destroy_entity_components(einfo);
	}
	pool_free(game.entities);

#if defined(HAS_LUA)
	script_exit();
#endif

	// TODO: deep deallocation
	// g_hash_table_destroy(game.subscriptions);

	// shutdown and free systems
	for (int i = game.reg_order->len - 1; i >= 0; i--) {
		ComponentType t = g_array_index(
			game.reg_order, ComponentType, i
		);
		struct System *sys = g_hash_table_lookup(
			game.systems, GINT_TO_POINTER(t)
		);

		if (sys->exit) {
			sys->exit(sys);
		}

		spark_log(
			LOG_DEBUG,
			"shut down '%s' system",
			sys->name
		);
		free(sys);
	}
	g_hash_table_destroy(game.systems);
	g_hash_table_destroy(game.sys_names_map);
	g_key_file_free(game.cfg);
	g_array_free(game.reg_order, true);
	g_array_free(game.hooks.entity_created, true);
	g_array_free(game.hooks.entity_destroyed, true);
	pool_free(game.event_queue);
}

EXPORT bool
spark_update()
{
#if defined(HAS_LUA)
	if (!script_update())
		return false;
#endif

	for (int i = 0; i < game.reg_order->len; i++) {
		ComponentType t = g_array_index(
			game.reg_order, ComponentType, i
		);
		struct System *sys = g_hash_table_lookup(
			game.systems, GINT_TO_POINTER(t)
		);

		if (sys->update)
			sys->update(sys);

		dispatch_events();
	}

	return true;
}

EXPORT void
spark_system_register(ComponentType t, struct System *s)
{
	g_assert_nonnull(s);
	g_assert_nonnull(s->name);
	g_assert_true(t != ANY_COMPONENT);

	g_hash_table_insert(game.systems, GINT_TO_POINTER(t), s);
	g_hash_table_insert(game.sys_names_map, s->name, GINT_TO_POINTER(t));
	g_array_append_val(game.reg_order, t);

	spark_log(LOG_DEBUG, "initialized '%s' system", s->name);

	if (s->init) {
		s->init(s);
	}
};

EXPORT void
spark_system_unregister(ComponentType t)
{
	g_assert_true(t != ANY_COMPONENT);

	struct System *s = g_hash_table_lookup(
		game.systems, GINT_TO_POINTER(t)
	);

	if (s) {
		// remove system components from all entities
		PoolIter i;
		pool_iter_init(game.entities, &i);
		while (pool_iter_next(&i)) {
			struct EntityInfo *ei = pool_iter_get(&i);
			spark_entity_component_remove(ei->entity, t);
		}

		// shutdown system and remove it from systems hash table
		if (s->exit)
			s->exit(s);
		g_hash_table_remove(game.systems, GINT_TO_POINTER(t));
		g_hash_table_remove(game.sys_names_map, s->name);

		// remove system also from the registered systems array
		for (int i = 0; i < game.reg_order->len; i++) {
			ComponentType ct = g_array_index(
				game.reg_order, ComponentType, i
			);

			if (ct == t) {
				g_array_remove_index(game.reg_order, i);
				break;
			}
		}

		g_free(s);
	}
}

EXPORT struct System*
spark_system_get(ComponentType t)
{
	g_assert_true(t != ANY_COMPONENT);
	return g_hash_table_lookup(game.systems, GINT_TO_POINTER(t));
}

EXPORT Entity
spark_entity_create(const ComponentType *components)
{
	Entity entity = pool_reserve(game.entities) + 1;
	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	einfo->entity = entity;
	einfo->components = g_hash_table_new(NULL, NULL);

	if (components) {
		while (*components) {
			spark_entity_component_add(entity, *components);
			components++;
		}
	}

	spark_log(LOG_DEBUG, "created entity %d", entity);

	// call the hooks
	for (int i = 0; i < game.hooks.entity_created->len; i++) {
		struct HookInfo *hi = &g_array_index(
			game.hooks.entity_created, struct HookInfo, i
		);
		hi->hook(HOOK_ENTITY_CREATED, hi->userdata);
	}

	return entity;
}

EXPORT void
spark_entity_destroy(Entity entity)
{
	assert_entity_is_valid(entity);
	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		spark_log(LOG_FATAL, "entity %d does not exist", entity);

	unsubscribe_entity(entity);
	destroy_entity_components(einfo);
	pool_remove(game.entities, entity - 1);

	spark_log(
		LOG_DEBUG,
		"destroyed entity %d",
		entity
	);

	for (int i = 0; i < game.hooks.entity_destroyed->len; i++) {
		struct HookInfo *hi = &g_array_index(
			game.hooks.entity_destroyed, struct HookInfo, i
		);
		hi->hook(HOOK_ENTITY_DESTROYED, hi->userdata);
	}
}


EXPORT void
spark_entity_component_add(Entity entity, ComponentType t)
{
	g_assert_true(t != ANY_COMPONENT);
	assert_entity_is_valid(entity);

	// get the components set for given entity
	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		spark_log(LOG_FATAL, "entity %d does not exist", entity);

	// ensure the component is not already possessed
	if (g_hash_table_contains(einfo->components, GINT_TO_POINTER(t))) {
		spark_log(
			LOG_FATAL,
			"entity %d already has the component %d",
			entity,
			t
		);
	}

	// lookup the component system and attempt to create a new component
	struct System *sys = g_hash_table_lookup(
		game.systems, GINT_TO_POINTER(t)
	);
	if (sys) {
		if (sys->create_component) {
			sys->create_component(sys, entity);
			g_hash_table_add(einfo->components, GINT_TO_POINTER(t));
			spark_log(
				LOG_DEBUG,
				"added component '%s' to entity %d",
				sys->name,
				entity
			);
		}
		else {
			spark_log(
				LOG_FATAL,
				"system '%s' does not allow component "
				"instantiation",
				sys->name
			);
		}
	}
	else {
		spark_log(LOG_FATAL, "unknown component %d",t);
	}
}


EXPORT void*
spark_entity_component_get(Entity entity, ComponentType t)
{
	g_assert_true(t != ANY_COMPONENT);
	assert_entity_is_valid(entity);

	if (!pool_get(game.entities, entity - 1))
		spark_log(LOG_FATAL, "entity %d does not exist", entity);

	// lookup the system and retrieve a pointer to component data
	struct System *sys = g_hash_table_lookup(
		game.systems, GINT_TO_POINTER(t)
	);
	if (sys) {
		if (sys->get_component) {
			void *c = sys->get_component(sys, entity);
			if (!c) {
				spark_log(
					LOG_FATAL,
					"entity %d does not have component '%s'",
					entity,
					sys->name
				);
			}
			return c;
		}
		else {
			spark_log(
				LOG_FATAL,
				"system '%s' does not allow component retrieval",
				sys->name
			);
		}
	}
	else {
		spark_log(LOG_FATAL, "unknown component %d", t);
	}
	return NULL;
}

EXPORT void
spark_entity_component_remove(Entity entity, ComponentType t)
{
	g_assert_true(t != ANY_COMPONENT);
	assert_entity_is_valid(entity);

	if (!pool_get(game.entities, entity - 1))
		spark_log(LOG_FATAL, "entity %d does not exist", entity);

	gpointer key = GUINT_TO_POINTER(t);

	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		spark_log(LOG_FATAL, "entity %d does not exist", entity);

	if (g_hash_table_contains(einfo->components, key)) {
		struct System *sys = g_hash_table_lookup(game.systems, key);
		if (sys->destroy_component)
			sys->destroy_component(sys, entity);
		g_hash_table_remove(einfo->components, key);
		unsubscribe_entity_component(entity, t);
	}
}

EXPORT bool
spark_entity_has_component(Entity entity, ComponentType t)
{
	g_assert_true(t != ANY_COMPONENT);
	assert_entity_is_valid(entity);

	gpointer key = GUINT_TO_POINTER(t);

	struct EntityInfo *einfo = pool_get(game.entities, entity - 1);
	if (!einfo)
		spark_log(LOG_FATAL, "entity %d does not exist", entity);

	return g_hash_table_contains(einfo->components, key);
}

EXPORT void
spark_log(int level, const char *fmt, ...)
{
	g_assert_nonnull(fmt);

	va_list ap;
	va_start(ap, fmt);

	if (level < game.log_level)
		return;

	switch (level) {
	case LOG_DEBUG:
	case LOG_INFO:
	case LOG_WARN:
		vprintf(fmt, ap);
		printf("\n");
		break;

	case LOG_ERROR:
	case LOG_FATAL:
		vfprintf(stderr, fmt, ap);
		fprintf(stderr, "\n");
		if (level == LOG_FATAL)
			abort();
		break;

	default:
		fprintf(stderr, "unknown log level\n");
		exit(EXIT_FAILURE);
	}
}

EXPORT void
spark_log_level_set(int level)
{
	g_assert_true(level >= 0 && level <= LOG_FATAL);
	game.log_level = level;
}

EXPORT void
spark_hook_register(int hook_type, Hook h, void *userdata)
{
	struct HookInfo hi = {
		.hook = h,
		.userdata = userdata
	};

	switch (hook_type) {
	case HOOK_ENTITY_CREATED:
		g_array_append_val(game.hooks.entity_created, hi);
		break;
	case HOOK_ENTITY_DESTROYED:
		g_array_append_val(game.hooks.entity_destroyed, hi);
		break;
	}
}
