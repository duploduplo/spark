#include "script_types/entity.h"
#include "defines.h"
#include "internals.h"
#include <glib.h>

#define SYMBOL_PATTERN "[A-z][A-z0-9_]*"

extern struct Game game;

typedef struct EntityObject {
	bool created;
	Entity entity;
	ComponentType *components;
} EntityObject;

static int entity_type_id = 0;
static int component_type_id = 0;

// GHashTable[component_type] -> GHashTable[name] -> MethodPrototype
static GHashTable *component_methods = NULL;

// GHashTable[component_type] -> GHashTable[name] -> Property
static GHashTable *component_props = NULL;

// TODO: remove these two functions and those in script.c and put them in a
// common place
static void
free_hash_table(gpointer ptr)
{
	g_hash_table_destroy(ptr);
}

static void
free_method_prototype(gpointer ptr)
{
	g_free(((MethodPrototype*)ptr)->argtv);
	g_free(ptr);
}

static Value
wrap_spark_entity_destroy(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv->val.object);
	spark_entity_destroy(e);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

/**
 * Wrapper for spark_entity_create()
 */
static Value
wrap_spark_entity_create(int argc, Value *argv)
{
	// return value
	Value v = {
		.type = SCRIPT_TYPE_NULL,
	};

	// array of deserialized component type identifiers
	ComponentType components[argc + 1];
	components[argc] = 0; // terminator

	// process arguments
	for (int i = 0; i < argc; i++) {
		// check argument type
		if (argv[i].type != SCRIPT_TYPE_STRING) {
			v.type = SCRIPT_TYPE_ERROR;
			v.val.string = g_strdup_printf(
				"arg %d: component type name must be a string", i
			);
			return v;
		}

		// lookup the corresponding component type identifier for the
		// given name
		components[i] = GPOINTER_TO_UINT(
			g_hash_table_lookup(game.sys_names_map, argv[i].val.string)
		);
		if (components[i] == 0) {
			v.type = SCRIPT_TYPE_ERROR;
			v.val.string = g_strdup_printf(
				"unknown component type '%s'",
				argv[i].val.string
			);
			return v;
		}
	}

	EntityObject* o = g_new0(EntityObject, 1);
	o->created = false;
	o->entity = 0;
	o->components = g_memdup(components, argc + 1);

	// return the object associated with given entity; after this, Lua
	// engine will call the constructor which will create the entity and
	// attach components
	v.type = SCRIPT_TYPE_OBJECT;
	v.val.object.type = entity_type_id;
	v.val.object.data = o;

	return v;
}

static void
script_attach_component(Object *o, ComponentType c)
{
	System *sys = spark_system_get(c);

	// set the component object attribute
	Value component_attr = {
		.type = SCRIPT_TYPE_OBJECT,
		.val.object = {
			.type = component_type_id,
			.data = o->data
		}
	};
	spark_script_object_attr_set(o, sys->name, &component_attr);

	// attach methods to component object
	GHashTable *methods = g_hash_table_lookup(
		component_methods, GUINT_TO_POINTER(c)
	);
	if (methods) {
		GHashTableIter i;
		gpointer k, v;

		g_hash_table_iter_init(&i, methods);
		while (g_hash_table_iter_next(&i, &k, &v)) {
			MethodPrototype *proto = v;

			lua_engine_attach_method(
				game.lua_engine,
				&component_attr.val.object,
				(char*)k,
				proto->method,
				proto->argc,
				proto->argtv
			);
		}
	}

	// attach properties to component object
	GHashTable *props = g_hash_table_lookup(
		component_props, GUINT_TO_POINTER(c)
	);
	if (props) {
		GHashTableIter i;
		gpointer k, v;

		g_hash_table_iter_init(&i, props);
		while (g_hash_table_iter_next(&i, &k, &v)) {
			Property *prop = v;

			lua_engine_define_property(
				game.lua_engine,
				component_attr.val.object.ref,
				(char*)k,
				prop
			);
		}
	}
}

/**
 * Stringify an Entity object.
 */
static char*
entity_str(Object *self)
{
	EntityObject *o = self->data;
	return g_strdup_printf("Entity %d", o->entity);
}

/**
 * Initialize an Entity object.
 */
static void
entity_ctor(Object *self)
{
	EntityObject *o = self->data;

	if (!o->created) {
		o->entity = spark_entity_create(o->components);
		o->created = true;
	}

	// attach the destroy() method
	int destroy_argtv[] = { SCRIPT_TYPE_OBJECT };
	lua_engine_attach_method(
		game.lua_engine,
		self,
		"destroy",
		wrap_spark_entity_destroy,
		1,
		destroy_argtv
	);

	if (!o->components)
		return;

	// attach components
	for (ComponentType *c = o->components; *c; c++) {
		script_attach_component(self, *c);
	}
}

/**
 * Destroy an Entity object.
 */
static void
entity_dtor(Object *self)
{
	EntityObject *o = self->data;
	g_free(o->components);

	// destroy the entity, if it still exists and has not been created by C
	if (!o->created) {
		EntityInfo *einfo = pool_get(game.entities, o->entity - 1);
		if (einfo)
			spark_entity_destroy(o->entity);
	}

	g_free(o);
	self->data = NULL;
}

void
entity_object_value_init(Value *v, Entity e)
{
	EntityObject* o = g_new0(EntityObject, 1);
	o->created = true;
	o->entity = e;
	o->components = NULL;

	EntityInfo *einfo = pool_get(game.entities, e - 1);
	if (einfo) {
		o->components = g_new0(ComponentType, g_hash_table_size(einfo->components) + 1);
		GHashTableIter i;
		int j = 0;
		gpointer k, v;
		g_hash_table_iter_init(&i, einfo->components);
		while (g_hash_table_iter_next(&i, &k, &v)) {
			o->components[j] = GPOINTER_TO_UINT(k);
			j++;
		}
	}

	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.type = entity_type_id;
	v->val.object.data = o;
}

void
register_entity_type()
{
	// TODO: clean-up these
	component_methods = g_hash_table_new_full(
		NULL, NULL, NULL, free_hash_table
	);
	component_props = g_hash_table_new_full(
		NULL, NULL, NULL, free_hash_table
	);

	TypeInfo component_ti = {
		.name = "Component",
		.ctor = NULL,
		.dtor = NULL,
		.str = NULL
	};
	component_type_id = spark_script_type_register(&component_ti);

	TypeInfo entity_ti = {
		.name = "Entity",
		.ctor = entity_ctor,
		.dtor = entity_dtor,
		.str = entity_str
	};

	entity_type_id = spark_script_type_register(&entity_ti);

	int arg_types[] = { SCRIPT_TYPE_VARARG };
	spark_script_func_register(
		"Entity",
		"new",
		wrap_spark_entity_create,
		1,
		arg_types
	);
}

EXPORT int
entity_object_type_id()
{
	return entity_type_id;
}

EXPORT void
entity_object_cmethod_register(
	ComponentType t,
	const char *name,
	Func f,
	int argc,
	int *argtv
) {
	g_assert_nonnull(name);
	g_assert_nonnull(argtv);
	g_assert_nonnull(f);

	if (!g_regex_match_simple(SYMBOL_PATTERN, name, 0, 0)) {
		spark_log(LOG_FATAL, "invalid component method name");
	}

	if (argc < 1 || argtv[0] != SCRIPT_TYPE_OBJECT) {
		spark_log(
			LOG_FATAL,
			"the component method must accept an entity object as first argument"
		);
	}

	// lookup or create the methods table for given component type
	GHashTable *methods = g_hash_table_lookup(
		component_methods, GUINT_TO_POINTER(t)
	);
	if (!methods) {
		methods = g_hash_table_new_full(
			g_str_hash,
			g_str_equal,
			g_free,
			free_method_prototype
		);
		g_hash_table_insert(component_methods, GUINT_TO_POINTER(t), methods);
	}

	// create a prototype descriptor and store it in methods table
	MethodPrototype *proto = g_new0(MethodPrototype, 1);
	proto->method = f;
	proto->argc = argc;
	proto->argtv = g_memdup(argtv, sizeof(int) * argc);

	g_hash_table_insert(methods, g_strdup(name), proto);
}

EXPORT void
entity_object_cproperty_register(
	ComponentType t,
	const char *name,
	Property *prop
) {
	GHashTable *props = g_hash_table_lookup(
		component_props, GUINT_TO_POINTER(t)
	);
	if (!props) {
		props = g_hash_table_new_full(
			g_str_hash,
			g_str_equal,
			g_free,
			g_free
		);
		g_hash_table_insert(component_props, GUINT_TO_POINTER(t), props);
	}

	g_hash_table_insert(props, g_strdup(name), g_memdup(prop, sizeof(Property)));
}

EXPORT Entity
entity_object_get_entity(Object *o)
{
	if (o->type != entity_type_id && o->type != component_type_id)
		return 0;

	return ((EntityObject*)o->data)->entity;
}
