#include "spark.h"

#include "internals.h"
#include "refcounter.h"
#include "script_types/array.h"
#include "script_types/entity.h"
#include "script_types/path.h"
#include "script_types/point.h"
#include "script_types/tile.h"
#include <glib.h>

static int path_type_id = 0;

static void
destroy_path(void *p)
{
	path_free(p);
}

static void
path_object_ctor(Object *self)
{
	self->type = path_type_id;
	if (!self->data)
		self->data = ref_counter_new(path_new(), destroy_path);
	else
		ref_counter_inc(self->data);
}

static void
path_object_dtor(Object *self)
{
	ref_counter_dec(self->data);
	self->data = NULL;
}

static Value
path_object_new(int argc, Value *argv)
{
	Value retval;

	if (argc != 1 ||
	    argv[0].val.object.type != array_object_type_id() ||
	    !array_object_has_type(&argv[0].val.object, SCRIPT_TYPE_OBJECT, point_object_type_id())) {
		retval.type = SCRIPT_TYPE_ERROR;
		retval.val.string = g_strdup(
			"bad arguments, expected an Array of Point objects"
		);
		return retval;
	}

	path_object_value_init(&retval, NULL);
	Path *path = ref_counter_get(retval.val.object.data);
	Object *array = &argv[0].val.object;

	for (int i = 0; i < array_object_len(array); i++) {
		Value *pt = array_object_index(array, i);
		int x, y;
		point_object_get(&pt->val.object, &x, &y);

		PathNode node = {
			.x = x,
			.y = y
		};
		path_node_add(path, node);
	}

	return retval;
}

static int
tile_cost_func(Tile *t, void *data)
{
	Value *callable = data;
	Value tile;

	// FIXME: refactor this
	tile_object_value_init(&tile, t, NULL, NULL);

	Value retval = spark_script_value_call(callable, 1, &tile);

	int cost = 0;
	switch (retval.type) {
	case SCRIPT_TYPE_INT:
		cost = retval.val.integer;
		break;
	case SCRIPT_TYPE_FLOAT:
		cost = retval.val.real;
		break;
	default:
		spark_log(LOG_FATAL, "the tile cost evaluation function must return a number");
	}

	return cost;
}

static Value
wrap_path_find(int argc, Value *argv)
{
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};

	// expect 1st argument to be an entity with a tilemap
	if (argv[0].val.object.type != entity_object_type_id() ||
	    !spark_entity_has_component(entity_object_get_entity(&argv[0].val.object), TILEMAP)) {
		retval.val.string = g_strdup(
			"bad arguments, first arg must be an entity with a tilemap component"
		);
		goto bad_args;
	}

	// expect 2nd and 3rd args to be point objects
	for (int i = 1; i < 3; i++) {
		if (argv[i].val.object.type != point_object_type_id()) {
			retval.val.string = g_strdup_printf(
				"bad arguments, argument %d must be a point",
				i + 1
			);
			goto bad_args;
		}
	}

	// get args and call path_find()
	Entity map = entity_object_get_entity(&argv[0].val.object);
	int x1, y1, x2, y2;
	point_object_get(&argv[1].val.object, &x1, &y1);
	point_object_get(&argv[2].val.object, &x2, &y2);
	Path *path = path_find(map, x1, y1, x2, y2, tile_cost_func, &argv[3]);

	// return the path, if any, null otherwise
	if (path) {
		path_object_value_init(&retval, path);
	}
	return retval;

bad_args:
	retval.type = SCRIPT_TYPE_ERROR;
	return retval;
}

EXPORT int
path_object_type_id()
{
	return path_type_id;
}

EXPORT void
path_object_value_init(Value *v, Path *path)
{
	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.type = path_type_id;

	if (path) {
		v->val.object.data = ref_counter_new(
			path_copy(path), destroy_path
		);
	}
	else {
		v->val.object.data = NULL;
		path_object_ctor(&v->val.object);
	}
}

EXPORT Path*
path_object_data(Object *o)
{
	return ref_counter_get(o->data);
}

void
register_path_type()
{
	TypeInfo ti = {
		.name = "Path",
		.ctor = path_object_ctor,
		.dtor = path_object_dtor,
		.str = NULL
	};

	path_type_id = spark_script_type_register(&ti);

	int new_argtv[] = { SCRIPT_TYPE_VARARG };
	spark_script_func_register("Path", "new", path_object_new, 1, new_argtv);

	int find_argtv[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_FUNCTION };
	spark_script_func_register("Path", "find", wrap_path_find, 4, find_argtv);
}
