#include "script_types/rect.h"
#include "script_types/tile.h"
#include <glib.h>

static int tile_type_id = 0;

typedef struct TileWrapper {
	Tile tile;
	void (*initializer)(Object*, void*);
	RefCounter *userdata_rc;
} TileWrapper;

static Value
tile_row_getter(Object *o)
{
	TileWrapper *tw = o->data;
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = tw->tile.row
	};
	return retval;
}

static Value
tile_column_getter(Object *o)
{
	TileWrapper *tw = o->data;
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = tw->tile.col
	};
	return retval;
}

static void
tile_ctor(Object *o)
{
	Property row_prop = {
		.type = SCRIPT_TYPE_INT,
		.setter = NULL,
		.getter = tile_row_getter
	};
	spark_script_object_prop_def(o, "row", &row_prop);

	Property column_prop = {
		.type = SCRIPT_TYPE_INT,
		.setter = NULL,
		.getter = tile_column_getter
	};
	spark_script_object_prop_def(o, "column", &column_prop);



	TileWrapper *tw = o->data;
	Value rect_val;
	Rect rect = tw->tile.rect;
	rect_object_value_init(
		&rect_val,
		rect.left,
		rect.top,
		rect_width(rect),
		rect_height(rect)
	);
	spark_script_object_attr_set(o, "rect", &rect_val);

	if (tw->initializer) {
		void *userdata = tw->userdata_rc ? ref_counter_get(tw->userdata_rc) : NULL;
		tw->initializer(o, userdata);
	}
}

static void
tile_dtor(Object *o)
{
	TileWrapper *tw = o->data;
	if (tw->userdata_rc) {
		ref_counter_dec(tw->userdata_rc);
		tw->userdata_rc = NULL;
	}
	g_free(tw);
	o->data = NULL;
}

static char*
tile_str(Object *o)
{
	TileWrapper *tw = o->data;
	return g_strdup_printf("Tile at %d,%d", tw->tile.col, tw->tile.row);
}

int
tile_object_type_id()
{
	return tile_type_id;
}

void
tile_object_value_init(
	Value *v,
	Tile *t,
	void (*initializer)(Object *o, void *userdata),
	RefCounter *userdata_rc
) {
	TileWrapper *tw = g_new0(TileWrapper, 1);
	tw->tile = *t;
	tw->tile.data = NULL;
	tw->initializer = initializer;
	tw->userdata_rc = userdata_rc;

	if (userdata_rc)
		ref_counter_inc(userdata_rc);

	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.type = tile_type_id;
	v->val.object.data = tw;
}

void
register_tile_type()
{
	TypeInfo tile_ti = {
		.name = "Tile",
		.ctor = tile_ctor,
		.dtor = tile_dtor,
		.str = tile_str
	};

	tile_type_id = spark_script_type_register(&tile_ti);
}
