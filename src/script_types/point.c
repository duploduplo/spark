#include "script_types/point.h"
#include <spark.h>
#include <glib.h>
#include <math.h>

static int point_type_id = 0;

static Value
x_setter(Object *self, Value *v)
{
	int *data = self->data;
	data[0] = v->val.integer;

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
x_getter(Object *self)
{
	int *data = self->data;
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = data[0]
	};
	return retval;
}

static Value
y_setter(Object *self, Value *v)
{
	int *data = self->data;
	data[1] = v->val.integer;

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
y_getter(Object *self)
{
	int *data = self->data;
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = data[1]
	};
	return retval;
}

static void
point_ctor(Object *self)
{
	if (!self->data) {
		self->type = point_type_id;
		self->data = g_new0(int, 2);
	}

	Property x_prop = {
		.type = SCRIPT_TYPE_FLOAT,
		.setter = x_setter,
		.getter = x_getter
	};
	spark_script_object_prop_def(self, "x", &x_prop);

	Property y_prop = {
		.type = SCRIPT_TYPE_FLOAT,
		.setter = y_setter,
		.getter = y_getter
	};
	spark_script_object_prop_def(self, "y", &y_prop);
}

static void
point_dtor(Object *self)
{
	g_free(self->data);
	self->data = NULL;
}

static char*
point_str(Object *self)
{
	g_assert_nonnull(self);
	g_assert_nonnull(self->data);
	int *pt = self->data;
	return g_strdup_printf("Point(%d, %d)", pt[0], pt[1]);
}

static Value
point_new(int argc, Value *argv)
{
	Value v;
	point_object_value_init(&v, argv[0].val.integer, argv[1].val.integer);
	return v;
}

static Value
point_distance(int argc, Value *argv)
{
	Value retval;

	Object *self = &argv[0].val.object;
	Object *other = &argv[1].val.object;

	if (other->type != point_type_id) {
		retval.type = SCRIPT_TYPE_ERROR;
		retval.val.string = g_strdup("the argument must be a Point object");
		return retval;
	}

	int x1, y1, x2, y2;
	point_object_get(self, &x1, &y1);
	point_object_get(other, &x2, &y2);

	retval.type = SCRIPT_TYPE_FLOAT;
	retval.val.real = sqrt(
		pow(x2 - x1, 2) + pow(y2 - y1, 2)
	);

	return retval;
}

void
register_point_type()
{
	TypeInfo ti = {
		.name = "Point",
		.ctor = point_ctor,
		.dtor = point_dtor,
		.str = point_str
	};

	point_type_id = spark_script_type_register(&ti);

	int new_argtv[] = { SCRIPT_TYPE_INT, SCRIPT_TYPE_INT };
	spark_script_func_register("Point", "new", point_new, 2, new_argtv);

	int distance_argtv[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_OBJECT };
	spark_script_type_method_register(
		point_type_id,
		"distance",
		point_distance,
		2,
		distance_argtv
	);

}

int
point_object_type_id()
{
	return point_type_id;
}

Object*
point_object_new(int x, int y)
{
	Object *o = g_new0(Object, 1);
	point_ctor(o);
	point_object_set(o, 0, 0);
	return o;
}

void
point_object_value_init(Value *v, int x, int y)
{
	g_assert_nonnull(v);
	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.data = NULL;
	point_ctor(&v->val.object);
	point_object_set(&v->val.object, x, y);
}

void
point_object_free(Object *o)
{
	g_assert_nonnull(o);
	point_dtor(o);
}

void
point_object_set(Object *o, int x, int y)
{
	g_assert_nonnull(o);
	g_assert_nonnull(o->data);

	int *pt = o->data;
	pt[0] = x;
	pt[1] = y;
}

void
point_object_get(Object *o, int *x, int *y)
{
	g_assert_nonnull(o);
	g_assert_nonnull(o->data);
	g_assert_nonnull(x);
	g_assert_nonnull(y);

	int *pt = o->data;
	*x = pt[0];
	*y = pt[1];
}
