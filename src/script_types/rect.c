#include "defines.h"
#include "script_types/rect.h"
#include <glib.h>

static int rect_type_id = 0;

static void
rect_ctor(Object *self)
{
	self->type = rect_type_id;
	if (!self->data) {
		self->data = g_new0(Rect, 1);
	}

	Rect *r = self->data;

	Value v = {
		.type = SCRIPT_TYPE_INT
	};

	v.val.integer = r->left;
	spark_script_object_attr_set(self, "x", &v);

	v.val.integer = r->top;
	spark_script_object_attr_set(self, "y", &v);

	v.val.integer = rect_ptr_width(r);
	spark_script_object_attr_set(self, "w", &v);

	v.val.integer = rect_ptr_height(r);
	spark_script_object_attr_set(self, "h", &v);
}

static void
rect_dtor(Object *self)
{
	g_free(self->data);
}

static char*
rect_str(Object *self)
{
	Rect *r = self->data;
	return g_strdup_printf(
		"Rect{%d, %d, %d, %d}",
		r->left,
		r->top,
		rect_ptr_width(r),
		rect_ptr_height(r)
	);
}

static Value
rect_new(int argc, Value *argv)
{
	Value retval;
	rect_object_value_init(
		&retval,
		argv[0].val.integer,
		argv[1].val.integer,
		argv[2].val.integer,
		argv[3].val.integer
	);

	return retval;
}

void
register_rect_type()
{
	TypeInfo ti = {
		.name = "Rect",
		.ctor = rect_ctor,
		.dtor = rect_dtor,
		.str = rect_str
	};

	rect_type_id = spark_script_type_register(&ti);

	int new_argtv[] = {
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT
	};
	spark_script_func_register(
		"Rect",
		"new",
		rect_new,
		4,
		new_argtv
	);
}

EXPORT int
rect_object_type_id()
{
	return rect_type_id;
}

EXPORT void
rect_object_value_init(Value *v, int x, int y, int w, int h)
{
	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.type = rect_type_id;
	v->val.object.data = NULL;

	v->val.object.data = g_new0(Rect, 1);

	Rect *r = v->val.object.data;
	r->top = y;
	r->left = x;
	r->bottom = y + h;
	r->right = x + w;
}

EXPORT Rect*
rect_object_get_rect(Object *o)
{
	return o->data;
}
