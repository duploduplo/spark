#include "common.h"
#include "systems/input.h"
#include <SDL_events.h>
#include <glib.h>

typedef struct Input {
	Entity entity;
	Rect area;
	int z;
	EventType input_type;
	bool forward;
} Input;

static void
input_system_init(struct System *self)
{
	self->data = NULL;
}

static void
input_system_exit(struct System *self)
{
	g_list_free(self->data);
}

static void
input_system_component_create(struct System *self, Entity e)
{
	Input input = { .entity = e };
	self->data = g_list_prepend(self->data, g_memdup(&input, sizeof(Input)));
}

static void*
input_system_get_component(System *self, Entity e)
{
		GList *l = g_list_first(self->data);
		while(l) {
			Input *input = l->data;
			if (input->entity == e) {
				return l->data;
			}
			l = g_list_next(l);
		}
	return NULL;
}

static void
input_system_component_destroy(struct System *self, Entity e)
{
	GList *l = g_list_first(self->data);
	while(l) {
		Input *input = l->data;
		if (input->entity == e) {
			self->data = g_list_delete_link(self->data, l);
			break;
		}
		l = g_list_next(l);
	}
}

static void
input_system_update(struct System *self)
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		void *evt;
		EventType type = 0;
		size_t size;

		switch (e.type) {
		case SDL_KEYUP:
		case SDL_KEYDOWN:
			if (e.type == SDL_KEYDOWN)
				type = EVENT_KEYPRESS;
			else
				type = EVENT_KEYRELEASE;

			size = sizeof(SDL_KeyboardEvent);
			evt = &e.key;
			break;

		case SDL_MOUSEBUTTONUP:
		case SDL_MOUSEBUTTONDOWN:
			if (e.type == SDL_MOUSEBUTTONDOWN)
				type = EVENT_MOUSEPRESS;
			else
				type = EVENT_MOUSERELEASE;

			size = sizeof(SDL_MouseButtonEvent);
			evt = &e.button;
			break;
		}

		if (type == 0)
			continue;

		GList *l = g_list_first(self->data);
		while(l) {
			Input *input = l->data;

			if (type & input->input_type && (
				type & EVENT_KEYPRESS ||
				type & EVENT_KEYRELEASE || (
				input->area.top <= e.button.y &&
				input->area.bottom >= e.button.y &&
				input->area.left <= e.button.x &&
				input->area.right >= e.button.x))) {

				// Push the event!
				spark_event_push(
					INPUT,
					type,
					input->entity,
					evt,
					size
				);

				if (!input->forward)
					break;
			}
			l = g_list_next(l);
		}
	}
}

gint
input_compare_z(gconstpointer a, gconstpointer b)
{
	Input *input_a = (Input *)a;
	Input *input_b = (Input *)b;
	// Reverse order by z
	if (input_a->z > input_b->z)
		return -1;
	else if (input_a->z < input_b->z)
		return +1;
	else
		return 0;
}

void
input_init(Entity e, Rect *area, int z_index, EventType input_type, bool forward)
{
	g_assert_nonnull(area);
	g_assert_true(area->left < area->right);
	g_assert_true(area->top < area->bottom);

	Input *input = spark_entity_component_get(e, INPUT);

	input->area.top = area->top;
	input->area.bottom = area->bottom;
	input->area.left = area->left;
	input->area.right = area->right;

	input->z = z_index;
	input->input_type = input_type;
	input->forward = forward;

	System *sys = spark_system_get(INPUT);
	sys->data = g_list_sort(sys->data, input_compare_z);
}

void
input_set_z(Entity e, int z_index)
{
	Input *input = spark_entity_component_get(e, INPUT);
	input->z = z_index;

	System *sys = spark_system_get(INPUT);
	sys->data = g_list_sort(sys->data, input_compare_z);
}

void
input_set_area(Entity e, Rect *area)
{
	g_assert_nonnull(area);
	g_assert_true(area->left < area->right);
	g_assert_true(area->top < area->bottom);

	Input *input = spark_entity_component_get(e, INPUT);

	input->area.top = area->top;
	input->area.bottom = area->bottom;
	input->area.left = area->left;
	input->area.right = area->right;
}

void
input_set_input_type(Entity e, EventType input_type)
{
	Input *input = spark_entity_component_get(e, INPUT);

	input->input_type = input_type;
}

void
input_set_forward(Entity e, bool forward)
{
	Input *input = spark_entity_component_get(e, INPUT);

	input->forward = forward;
}

extern void
input_system_register_wrappers();

void
input_system_register()
{
	spark_assert_has_systems(CORE);

	System *sys = g_new0(struct System, 1);
	sys->name = "input";
	sys->init = input_system_init;
	sys->exit = input_system_exit;
	sys->update = input_system_update;
	sys->create_component = input_system_component_create;
	sys->get_component = input_system_get_component;
	sys->destroy_component = input_system_component_destroy;
	spark_system_register(INPUT, sys);

	input_system_register_wrappers();
}
