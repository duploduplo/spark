#include "script.h"
#include "script_types/array.h"
#include "script_types/entity.h"
#include "script_types/point.h"
#include "script_types/rect.h"
#include "systems.h"
#include "systems/sprite.h"
#include "systems/sprite_internals.h"
#include <SDL2/SDL_rect.h>
#include <glib.h>

#define reg_wrapper(f, ...) {\
	char *name = #f;\
	int argtv[] = { SCRIPT_TYPE_OBJECT, __VA_ARGS__ };\
	int argc = sizeof(argtv) / sizeof(int);\
	entity_object_cmethod_register(SPRITE, name + 12, f, argc, argtv);\
}

#define reg_property(name, t) {\
	Property def = {\
		.type = t,\
		.setter = name ## _setter,\
		.getter = name ## _getter\
	};\
	entity_object_cproperty_register(SPRITE, #name, &def);\
}

static Value
wrap_sprite_init(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	sprite_init(e, argv[1].val.string);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
wrap_sprite_clone(int argc, Value *argv)
{
	Entity dst = entity_object_get_entity(&argv[0].val.object);
	Entity src = entity_object_get_entity(&argv[1].val.object);

	sprite_clone(dst, src);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
wrap_sprite_animation_add(int argc, Value *argv)
{
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	Entity e = entity_object_get_entity(&argv[0].val.object);
	Object *frames = &argv[2].val.object;

	// check frames array argument
	bool is_array = frames->type == array_object_type_id();
	if (!is_array)
		goto bad_args;

	bool is_array_of_ints = true;
	int len = array_object_len(frames);

	for (int i = 0; i < len; i++) {
		Value *element = array_object_index(frames, i);

		if (element->type != SCRIPT_TYPE_INT) {
			is_array_of_ints = false;
			break;
		}
	}
	if (!is_array_of_ints)
		goto bad_args;

	int *frames_array = g_new(int, len);
	for (int i = 0; i < len; i++) {
		frames_array[i] = array_object_index(frames, i)->val.integer;
	}

	sprite_animation_add(
		e,
		argv[1].val.string,
		frames_array,
		len,
		argv[3].val.integer
	);

	return retval;

bad_args:
	retval.type = SCRIPT_TYPE_ERROR;
	retval.val.string = g_strdup(
		"frames argument must be an Array of integers"
	);

	return retval;
}

static Value
wrap_sprite_animation_play(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	sprite_animation_play(e, argv[1].val.string);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
wrap_sprite_animation_pause(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	sprite_animation_pause(e);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
wrap_sprite_animation_is_paused(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	Value retval = {
		.type = SCRIPT_TYPE_BOOL,
		.val.integer = sprite_animation_is_paused(e)
	};
	return retval;
}

static Value
wrap_sprite_animation_resume(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	sprite_animation_resume(e);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
frameset_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};

	Object *array_obj = &v->val.object;
	bool is_array = array_obj->type == array_object_type_id();
	if (!is_array)
		goto bad_args;

	bool is_array_of_rects = true;
	int frames = array_object_len(array_obj);

	for (int i = 0; i < frames; i++) {
		Value *element = array_object_index(array_obj, i);

		if (element->type != SCRIPT_TYPE_OBJECT ||
		    element->val.object.type != rect_object_type_id()) {
			is_array_of_rects = false;
			break;
		}
	}
	if (!is_array_of_rects)
		goto bad_args;

	// populate a raw array of integers, which describes the frameset, by
	// iterating over the array of rects
	int *frameset = g_new(int, frames * 4);
	for (int i = 0; i < frames; i++) {
		Rect *r = rect_object_get_rect(
			&array_object_index(array_obj, i)->val.object
		);
		frameset[i * 4 + 0] = r->left;
		frameset[i * 4 + 1] = r->top;
		frameset[i * 4 + 2] = r->right - r->left;
		frameset[i * 4 + 3] = r->bottom - r->top;
	}

	sprite_frameset_set(e, frameset, frames);
	g_free(frameset);
	return retval;

bad_args:
	retval.type = SCRIPT_TYPE_ERROR;
	retval.val.string = g_strdup(
		"frameset argument must be an Array of Rect objects"
	);
	return retval;
}

static Value
frameset_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	Sprite *spr = spark_entity_component_get(e, SPRITE);

	int frames = spr->frameset->len;
	Value frameset[frames];
	for (int f = 0; f < frames; f++) {
		SDL_Rect *rect = &g_array_index(spr->frameset, SDL_Rect, f);
		rect_object_value_init(
			frameset + f, rect->x, rect->y, rect->w, rect->h
		);
	}

	Value retval = {
		.type = SCRIPT_TYPE_OBJECT,
	};
	array_object_value_init(&retval, frames, frameset);

	return retval;
}


static Value
x_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	int x, y;
	sprite_position_get(e, &x, &y);
	sprite_position_set(e, v->val.integer, y);
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
x_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	int x, y;
	sprite_position_get(e, &x, &y);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = x
	};
	return retval;
}

static Value
y_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	int x, y;
	sprite_position_get(e, &x, &y);
	sprite_position_set(e, x, v->val.integer);
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
y_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	int x, y;
	sprite_position_get(e, &x, &y);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = y
	};
	return retval;
}


static Value
visible_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	sprite_visible_set(e, v->val.integer);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
visible_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	Value retval = {
		.type = SCRIPT_TYPE_BOOL,
		.val.integer = sprite_visible_get(e)
	};
	return retval;
}


static Value
z_setter(Object *o, Value *v)
{
	Entity e = entity_object_get_entity(o);
	sprite_zorder_set(e, v->val.integer);
	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
z_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);
	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = sprite_zorder_get(e)
	};
	return retval;
}

void
sprite_system_register_wrappers()
{
	reg_wrapper(wrap_sprite_init, SCRIPT_TYPE_STRING);
	reg_wrapper(wrap_sprite_clone, SCRIPT_TYPE_OBJECT);
	reg_wrapper(wrap_sprite_animation_add, SCRIPT_TYPE_STRING, SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_INT);
	reg_wrapper(wrap_sprite_animation_play, SCRIPT_TYPE_STRING);
	reg_wrapper(wrap_sprite_animation_pause);
	reg_wrapper(wrap_sprite_animation_is_paused);
	reg_wrapper(wrap_sprite_animation_resume);

	reg_property(frameset, SCRIPT_TYPE_OBJECT);
	reg_property(x, SCRIPT_TYPE_INT);
	reg_property(y, SCRIPT_TYPE_INT);
	reg_property(visible, SCRIPT_TYPE_BOOL);
	reg_property(z, SCRIPT_TYPE_INT);
}
