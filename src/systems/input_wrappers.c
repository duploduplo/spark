#include "script_types/entity.h"
#include "systems.h"
#include "systems/input.h"
#include <SDL2/SDL_events.h>
#include <glib.h>

static int mouse_event_type_id = 0;

static void
mouse_event_ctor(Object *o)
{
	if (o->data) {
		SDL_MouseButtonEvent *evt = o->data;
		Value x = {
			.type = SCRIPT_TYPE_INT,
			.val.integer = evt->x
		};
		Value y = {
			.type = SCRIPT_TYPE_INT,
			.val.integer = evt->y
		};
		Value button = {
			.type = SCRIPT_TYPE_STRING,
			.val.string = g_strdup(
				evt->button == SDL_BUTTON_LEFT ?
				"Left" :
				"Right"
			)
		};

		spark_script_object_attr_set(o, "x", &x);
		spark_script_object_attr_set(o, "y", &y);
		spark_script_object_attr_set(o, "button", &button);

		o->data = NULL;
	}
}

static void
mouse_event_object_value_init(Value *v, SDL_MouseButtonEvent *evt)
{
	v->type = SCRIPT_TYPE_OBJECT;
	v->val.object.type = mouse_event_type_id;
	v->val.object.data = evt;
}

static void
handle_mouse(Event *evt, void *data)
{
	Value f = {
		.type = SCRIPT_TYPE_FUNCTION,
		.val.funcref = GPOINTER_TO_INT(data)
	};

	Value event_info;
	mouse_event_object_value_init(&event_info, evt->data);
	spark_script_value_call(&f, 1, &event_info);
}

static void
handle_key(Event *evt, void *data)
{
	Value f = {
		.type = SCRIPT_TYPE_FUNCTION,
		.val.funcref = GPOINTER_TO_INT(data)
	};

	Value key = {
		.type = SCRIPT_TYPE_STRING,
		.val.string = g_strdup(SDL_GetKeyName(((SDL_KeyboardEvent*)evt->data)->keysym.sym))
	};

	spark_script_value_call(&f, 1, &key);
}

static Value
on_mouse_press(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		INPUT,
		EVENT_MOUSEPRESS,
		e,
		handle_mouse,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
on_mouse_release(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		INPUT,
		EVENT_MOUSERELEASE,
		e,
		handle_mouse,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
on_key_press(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		INPUT,
		EVENT_KEYPRESS,
		e,
		handle_key,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
on_key_release(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);

	spark_event_subscribe(
		INPUT,
		EVENT_KEYRELEASE,
		e,
		handle_key,
		GINT_TO_POINTER(argv[1].val.funcref)
	);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

void
input_system_register_wrappers()
{
	TypeInfo mouse_event_ti = {
		.name = "MouseEvent",
		.ctor = mouse_event_ctor,
		.dtor = NULL,
		.str = NULL
	};
	mouse_event_type_id = spark_script_type_register(&mouse_event_ti);

	int argtv[] = { SCRIPT_TYPE_OBJECT, SCRIPT_TYPE_FUNCTION };
	entity_object_cmethod_register(
		INPUT,
		"on_key_press",
		on_key_press,
		2,
		argtv
	);
	entity_object_cmethod_register(
		INPUT,
		"on_key_release",
		on_key_release,
		2,
		argtv
	);

	entity_object_cmethod_register(
		INPUT,
		"on_mouse_press",
		on_mouse_press,
		2,
		argtv
	);
	entity_object_cmethod_register(
		INPUT,
		"on_mouse_release",
		on_mouse_release,
		2,
		argtv
	);
}
