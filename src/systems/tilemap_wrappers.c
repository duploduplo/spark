#include "script.h"
#include "script_types/entity.h"
#include "script_types/tile.h"
#include "systems/tilemap.h"
#include <glib.h>

#define reg_wrapper(f, ...) {\
	char *name = #f;\
	int argtv[] = { SCRIPT_TYPE_OBJECT, __VA_ARGS__ };\
	int argc = sizeof(argtv) / sizeof(int);\
	entity_object_cmethod_register(TILEMAP, name + 5, f, argc, argtv);\
}

static void
update_func(int update_type, struct Tile *t, void *user_data)
{
	char *upd_type;
	switch (update_type) {
	case TILE_CREATE:
		upd_type = "create";
		break;
	case TILE_UPDATE:
		upd_type = "update";
		break;
	case TILE_DESTROY:
		upd_type = "destroy";
		break;
	}

	Value args[2] = {{
		.type = SCRIPT_TYPE_STRING,
		.val.string = upd_type
	}, {
		.type = SCRIPT_TYPE_OBJECT,
	}};
	tile_object_value_init(&args[1], t, NULL, NULL);
	spark_script_value_call(user_data, 2, args);
}

static Value
n_rows_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);

	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = tilemap_n_rows_get(e)
	};
	return retval;
}

static Value
n_cols_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);

	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = tilemap_n_cols_get(e)
	};
	return retval;
}

static Value
tile_width_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);

	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = tilemap_tile_width_get(e)
	};
	return retval;
}

static Value
tile_height_getter(Object *o)
{
	Entity e = entity_object_get_entity(o);

	Value retval = {
		.type = SCRIPT_TYPE_INT,
		.val.integer = tilemap_tile_height_get(e)
	};
	return retval;
}

static Value
wrap_init(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);
	unsigned short tile_width = argv[1].val.integer;
	unsigned short tile_height = argv[2].val.integer;
	unsigned int cols = argv[3].val.integer;
	unsigned int rows = argv[4].val.integer;

	RefCounter *callable_rc = ref_counter_new(
		g_memdup(&argv[5], sizeof(Value)), g_free
	);

	tilemap_init(e, tile_width, tile_height, cols, rows, update_func, callable_rc);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

static Value
wrap_tile_at_point(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);
	int x = argv[1].val.integer;
	int y = argv[2].val.integer;

	Value retval;
	Tile *t = tilemap_tile_at_point(e, x, y);
	// FIXME: call initializer on this
	tile_object_value_init(&retval, t, NULL, NULL);

	return retval;
}

static Value
wrap_tile_update(int argc, Value *argv)
{
	Entity e = entity_object_get_entity(&argv[0].val.object);
	int col = argv[1].val.integer;
	int row = argv[2].val.integer;

	tilemap_tile_update(e, col, row);

	Value retval = {
		.type = SCRIPT_TYPE_NULL
	};
	return retval;
}

void
tilemap_system_register_wrappers()
{
	reg_wrapper(
		wrap_init,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_FUNCTION
	);

	reg_wrapper(
		wrap_tile_at_point,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT
	);

	reg_wrapper(
		wrap_tile_update,
		SCRIPT_TYPE_INT,
		SCRIPT_TYPE_INT
	);

	Property tile_width_prop = {
		.type = SCRIPT_TYPE_INT,
		.setter = NULL,
		.getter = tile_width_getter
	};
	entity_object_cproperty_register(
		TILEMAP,
		"tile_width",
		&tile_width_prop
	);

	Property tile_height_prop = {
		.type = SCRIPT_TYPE_INT,
		.setter = NULL,
		.getter = tile_height_getter
	};
	entity_object_cproperty_register(
		TILEMAP,
		"tile_height",
		&tile_height_prop
	);

	Property num_rows_prop = {
		.type = SCRIPT_TYPE_INT,
		.setter = NULL,
		.getter = n_rows_getter
	};
	entity_object_cproperty_register(
		TILEMAP,
		"rows",
		&num_rows_prop
	);

	Property num_cols_prop = {
		.type = SCRIPT_TYPE_INT,
		.setter = NULL,
		.getter = n_cols_getter
	};
	entity_object_cproperty_register(
		TILEMAP,
		"columns",
		&num_cols_prop
	);
}
