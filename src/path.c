#include "path.h"
#include <AStar.h>
#include <glib.h>
#include <math.h>

struct AStarContext {
	ASPath astar_path;
	Entity map;
	TileCostFunc tile_cost;
	void *data;
};

struct Path {
	GArray *nodes;
};

static void
node_neighbors(ASNeighborList neighbors, void *node, void *context)
{
	struct AStarContext *ctx = context;
	Entity map = ctx->map;
	PathNode *n = node;
	struct Tile *t = tilemap_tile_at_point(map, n->x, n->y);

	static const int directions[] = {
		 0,  0,
		 0,  1,
		 0, -1,
		 1,  0,
		 1,  1,
		 1, -1,
		-1,  0,
		-1,  1,
		-1, -1
	};

	int w = rect_width(t->rect);
	int h = rect_height(t->rect);
	int x = n->x;
	int y = n->y;

	for (int i = 0; i < 8; i++) {
		int x_dir = directions[i * 2];
		int y_dir = directions[i * 2 + 1];
		int x1 = x + w * x_dir;
		int y1 = y + h * y_dir;
		struct Tile *neighbor = tilemap_tile_at_point(map, x1, y1);

		int cost = ctx->tile_cost(neighbor, ctx->data);

		if (cost >= 0) {
			float factor = (abs(x_dir) + abs(y_dir)) < 2 ? 1 : 1.414;
			PathNode n1 = {
				.x = x1,
				.y = y1
			};
			ASNeighborListAdd(neighbors, &n1, cost * factor);
		}
	}
}

static float
path_cost_heuristic(void *from, void *to, void *context)
{
	PathNode *a = from;
	PathNode *b = to;
	int cost = (abs(b->y - a->y) + abs(b->x - a->x)) / 24;
	return cost;
}

Path*
path_new()
{
	Path *p = g_new(Path, 1);
	p->nodes = g_array_new(false, false, sizeof(PathNode));
	return p;
}

void
path_node_add(Path *p, PathNode n)
{
	g_assert_nonnull(p);
	g_array_append_val(p->nodes, n);
}

Path*
path_find(
	Entity map,
	int x1,
	int y1,
	int x2,
	int y2,
	TileCostFunc cost_func,
	void *data
) {
	g_assert_true(spark_entity_has_component(map, TILEMAP));
	g_assert_nonnull(cost_func);

	ASPathNodeSource pns = {
		.nodeSize = sizeof(PathNode),
		.nodeNeighbors = node_neighbors,
		.pathCostHeuristic = path_cost_heuristic,
		.earlyExit = NULL,
		.nodeComparator = NULL
	};

	struct Tile *t1 = tilemap_tile_at_point(map, x1, y1);
	PathNode n1 = {
		.x = t1->rect.left + rect_width(t1->rect) / 2,
		.y = t1->rect.top + rect_height(t1->rect) / 2
	};

	struct Tile *t2 = tilemap_tile_at_point(map, x2, y2);
	PathNode n2 = {
		.x = t2->rect.left + rect_width(t2->rect) / 2,
		.y = t2->rect.top + rect_height(t2->rect) / 2
	};

	struct AStarContext *ctx = g_new(struct AStarContext, 1);
	ctx->map = map;
	ctx->tile_cost = cost_func;
	ctx->data = data;
	ctx->astar_path = ASPathCreate(&pns, ctx, &n1, &n2);

	Path *path = path_new();
	for (int i = 0; i < ASPathGetCount(ctx->astar_path); i++) {
		PathNode *node = ASPathGetNode(ctx->astar_path, i);
		path_node_add(path, *node);
	}
	ASPathDestroy(ctx->astar_path);
	g_free(ctx);

	// adjust endpoints
	if (path->nodes->len) {
		PathNode *start = path_node_get(path, 0);
		PathNode *end = path_node_get(path, path->nodes->len - 1);
		start->x = x1; start->y = y1;
		end->x = x2; end->y = y2;
	}

	return path;
}

size_t
path_len(Path *path)
{
	return path->nodes->len;
}

PathNode*
path_node_get(Path *path, size_t index)
{
	g_assert_true(index < path->nodes->len);
	return &g_array_index(path->nodes, PathNode, index);
}

Path*
path_copy(Path *path)
{
	Path *copy = path_new();
	for (int i = 0; i < path->nodes->len; i++)
		path_node_add(copy, *path_node_get(path, i));
	return copy;
}

void
path_free(Path *path)
{
	g_array_free(path->nodes, true);
	g_free(path);
}
