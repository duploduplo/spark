# Spark ECS engine

## Requirements

* GLib library and header files
* A C99-compatible compiler (CLang / GCC works fine)
* CMake 3.0 
* SDL-2.0 and SDL_Image-2.0 (if build of systems is enabled)


## Building

First, clone and checkout the Git submodules for the dependencies:

    $ git submodule init
    $ git submodule update

Create a build directory, to keep source tree clean:

    $ mkdir build
    $ cd build

Bootstrap the build system:

    $ cmake ..

The following options can be given to cmake with `-D<OPTION>=VALUE` arguments to
tweak the build configuration:

    BUILD_SYSTEMS   toggles the build of engine's systems (enabled)
    BUILD_TESTS     toggles the build of unit tests suite (enabled)

Then just issue:

    $ make


## Tests

Once the project is built, tests, if built, can be run from build directory,
typically:

    $ build/tests/spark-tests
