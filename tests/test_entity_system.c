#include "spark.h"
#include "containers/mapped_pool.h"
#include <glib.h>
#include <stdbool.h>
#include <stdio.h>

#define ENTITY_COMPONENT 2

extern struct Game game;

static int entities_count = 0;

struct ComponentWithEntity {
	Entity e;
	Entity child;
};


struct EntitySystem {
	MappedPool *components;
	int counter;
};

static void
sys_init(struct System *self)
{
	struct EntitySystem *sys = g_new(struct EntitySystem, 1);
	sys->components = mapped_pool_new(sizeof(struct ComponentWithEntity), 5, 5);
	sys->counter = 0;
	self->data = sys;
}

static void
sys_exit(struct System *self)
{
	struct EntitySystem *sys = self->data;
	mapped_pool_free(sys->components);
	g_free(sys);
}

static void
sys_update(struct System *self)
{
	struct EntitySystem *sys = spark_system_get(ENTITY_COMPONENT)->data;
	Pool *p = mapped_pool_storage_get(sys->components);
	PoolIter i;
	pool_iter_init(p, &i);
	while (pool_iter_next(&i)) {
		// TODO
	}
}

static void
sys_create_component(struct System *self, Entity entity)
{
	struct EntitySystem *sys = self->data;
	struct ComponentWithEntity c = {
		.e = entity,
		.child = spark_entity_create(NULL)
	};
	mapped_pool_set(sys->components, entity, &c);
}

static void
sys_destroy_component(struct System *self, Entity entity)
{
	struct EntitySystem *sys = self->data;
	struct ComponentWithEntity *c = mapped_pool_get(sys->components, entity);
	spark_entity_destroy(c->child);
	mapped_pool_pop(sys->components, entity);
}

static void*
sys_get_component(struct System *self, Entity entity)
{
	struct EntitySystem *sys = self->data;
	return mapped_pool_get(sys->components, entity);
}

static void
hook(int type, void *data)
{
	if (type == HOOK_ENTITY_CREATED)
		entities_count++;
	else if (type == HOOK_ENTITY_DESTROYED)
		entities_count--;
}

static void
setup(gpointer fixture, gconstpointer user_data)
{
	struct System *sys = g_new0(struct System, 1);
	sys->name = "entity";
	sys->init = sys_init;
	sys->exit = sys_exit;
	sys->update = sys_update;
	sys->create_component = sys_create_component;
	sys->destroy_component = sys_destroy_component;
	sys->get_component = sys_get_component;

	spark_init(NULL);
	spark_system_register(ENTITY_COMPONENT, sys);
	spark_hook_register(HOOK_ENTITY_CREATED, hook, NULL);
	spark_hook_register(HOOK_ENTITY_DESTROYED, hook, NULL);

	entities_count = 0;
}

static void
teardown(gpointer fixture, gconstpointer user_data)
{
	spark_exit();
}

static void
test_component_with_empty_entity(gpointer fixture, gconstpointer user_data)
{
	ComponentType components[] = { ENTITY_COMPONENT, 0 };
	Entity entities[100];
	struct EntitySystem *sys = spark_system_get(ENTITY_COMPONENT)->data;
	Pool *pool = mapped_pool_storage_get(sys->components);


	for (int i = 0; i < 100; i++) {
		entities[i] = spark_entity_create(components);
	}
	g_assert_true(entities_count == 200);
	g_assert_cmpuint(pool_len(pool), ==, 100);

	for (int i = 0; i < 100; i++) {
		spark_entity_destroy(entities[i]);
	}
	g_assert_true(entities_count == 0);
}

void
entity_system_suite_setup()
{
	g_test_add(
		"/entity_system/component_with_empty_entity",
		void,
		NULL,
		setup,
		test_component_with_empty_entity,
		teardown
	);
}
