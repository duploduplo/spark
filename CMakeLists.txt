cmake_minimum_required(VERSION 3.2)

project(spark)

option(BUILD_SYSTEMS "build engine's systems" ON)
option(BUILD_TESTS "build tests suite" ON)
option(ENABLE_LUA "enable Lua scripting" ON)

set(
	COMPILE_OPTIONS
	-Wall
	-std=c99
)


include(FindPkgConfig)

# Find GLib package
pkg_search_module(GLIB REQUIRED glib-2.0)
include_directories(${GLIB_INCLUDE_DIRS})
link_directories(${GLIB_LIBRARY_DIRS})
set(LIBRARIES ${GLIB_LIBRARIES})

# Find Lua
if(${ENABLE_LUA})
	find_package(Lua 5.0 REQUIRED)
	add_definitions(-DHAS_LUA)
	include_directories(${LUA_INCLUDE_DIR})
	list(APPEND LIBRARIES ${LUA_LIBRARIES})
endif()

if(${BUILD_SYSTEMS})
	# Find SDL2 and SDL2_Image
	pkg_search_module(SDL2 REQUIRED sdl2)
	pkg_search_module(SDL2_IMAGE REQUIRED SDL2_image>=2.0.0)

	# Build AStar dependency ("astar" target)
	set(ASTAR_DIR ${PROJECT_SOURCE_DIR}/deps/AStar)
	set(ASTAR_BUILD_DIR ${PROJECT_BINARY_DIR}/astar)
	add_library(astar SHARED EXCLUDE_FROM_ALL ${ASTAR_DIR}/AStar.c)
	set_target_properties(
		astar PROPERTIES
		COMPILE_FLAGS "-std=c99"
		LIBRARY_OUTPUT_DIRECTORY ${ASTAR_BUILD_DIR}
	)

	# Build Chipmunk2D dependency ("chipmunk" target)
	set(CHIPMUNK_DIR ${PROJECT_SOURCE_DIR}/deps/Chipmunk2D)
	set(CHIPMUNK_BUILD_DIR ${PROJECT_BINARY_DIR}/chipmunk)
	set(CHIPMUNK_LIB ${CHIPMUNK_BUILD_DIR}/src/${CMAKE_SHARED_LIBRARY_PREFIX}chipmunk${CMAKE_SHARED_LIBRARY_SUFFIX})
	add_custom_command(
		OUTPUT ${CHIPMUNK_LIB}
		COMMAND ${CMAKE_COMMAND}
			-D INSTALL_DEMOS=OFF
			-D BUILD_DEMOS=OFF
			-D BUILD_STATIC=OFF
			-D INSTALL_STATIC=OFF
			${CHIPMUNK_DIR}
		COMMAND make
		WORKING_DIRECTORY ${CHIPMUNK_BUILD_DIR}
		VERBATIM
	)
	add_custom_target(chipmunk_build_dir COMMAND ${CMAKE_COMMAND} -E make_directory ${CHIPMUNK_BUILD_DIR})
	add_custom_target(chipmunk DEPENDS chipmunk_build_dir ${CHIPMUNK_LIB})

	# Build tmx dependency ("tmx" target)
	find_package(LibXml2)
	find_package(ZLIB)
	set(TMX_DIR ${PROJECT_SOURCE_DIR}/deps/tmx)
	set(TMX_BUILD_DIR ${PROJECT_BINARY_DIR}/tmx)
	set(TMX_LIB ${TMX_BUILD_DIR}/${CMAKE_SHARED_LIBRARY_PREFIX}tmx${CMAKE_SHARED_LIBRARY_SUFFIX})
	add_custom_command(
		OUTPUT ${TMX_LIB}
		COMMAND ${CMAKE_COMMAND}
			-D WANT_JSON=OFF
			-D BUILD_SHARED_LIBS=ON
			${TMX_DIR}
		COMMAND make
		WORKING_DIRECTORY ${TMX_BUILD_DIR}
		VERBATIM
	)
	add_custom_target(tmx_build_dir COMMAND ${CMAKE_COMMAND} -E make_directory ${TMX_BUILD_DIR})
	add_custom_target(tmx DEPENDS tmx_build_dir ${TMX_LIB})

	# Add deps include dirs
	include_directories(
		${SDL2_INCLUDE_DIRS}
		${SDL2_IMAGE_INCLUDE_DIRS}
		${LIBXML2_INCLUDE_DIRS}
		${ZLIB_INCLUDE_DIRS}
		${CHIPMUNK_DIR}/include
		${ASTAR_DIR}
		${TMX_DIR}/src
	)

	# Add deps libraries
	link_directories(
		${SDL2_LIBRARY_DIRS}
		${SDL2_IMAGE_LIBRARY_DIRS}
		${LIBXML2_LIBRARY_DIRS}
	)
	list(
		APPEND LIBRARIES
		astar
		${CHIPMUNK_LIB}
		${TMX_LIB}
		${SDL2_LIBRARIES}
		${SDL2_IMAGE_LIBRARIES}
		${LIBXML2_LIBRARIES}
		${ZLIB_LIBRARIES}
	)

	set(DEPENDENCIES astar chipmunk tmx)
endif()

include_directories(include include/spark)
add_subdirectory(src)

if(${BUILD_TESTS})
	add_subdirectory(tests)
endif()
