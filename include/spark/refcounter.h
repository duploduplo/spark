/**
 * @file
 * @defgroup util util
 * Utilities
 * @{
 */

#pragma once

struct RefCounter;

/**
 * Opaque type which provides a simple reference counting mechanism.
 */
typedef struct RefCounter RefCounter;

/**
 * Create a reference counter.
 */
RefCounter*
ref_counter_new(void *data, void (*destructor)(void*));

/**
 * Increment reference.
 */
RefCounter*
ref_counter_inc(RefCounter *rc);

/**
 * Decrement reference.
 */
void
ref_counter_dec(RefCounter *rc);

/**
 * Retrieve data.
 */
void*
ref_counter_get(RefCounter *rc);

/** @} */
