#pragma once

#include "systems/tilemap.h"

typedef void* TMXPropertyMap;

typedef struct TMXTile {
	Entity entity;
	TMXPropertyMap props;
} TMXTile;

Entity
tmx_map_load(
	const char *filename,
	TileUpdateFunc f,
	RefCounter *user_data
);

void
tmx_map_loader_register();
