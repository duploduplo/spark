#pragma once

#include "spark.h"

// component types
enum {
	CORE = ANY_COMPONENT + 1,
	BODY,
	GFX,
	IMAGE,
	INPUT,
	MOVABLE,
	SPRITE,
	TILEMAP,
	TIMER
};

