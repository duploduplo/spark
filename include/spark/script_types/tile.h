#pragma once

#include "refcounter.h"
#include "script.h"
#include "systems/tilemap.h"

int
tile_object_type_id();

void
tile_object_value_init(
	Value *v,
	Tile *t,
	void (*initializer)(Object *o, void *userdata),
	RefCounter *userdata_rc
);
