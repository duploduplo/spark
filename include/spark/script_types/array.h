#pragma once

#include "script.h"
#include <stddef.h>

int
array_object_type_id();

void
array_object_value_init(Value *v, int len, Value *elements);

Value*
array_object_index(Object *o, int index);

size_t
array_object_len(Object *o);

int
array_object_has_type(Object *o, int type, int obj_type);
