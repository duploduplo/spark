#pragma once

#include "spark.h"
#include "script.h"

int
entity_object_type_id();

/**
 * Register a component method.
 *
 * An entity object which has a component of given type will be dynamically
 * decorated with the given method.
 */
void
entity_object_cmethod_register(
	ComponentType t,
	const char *name,
	Func f,
	int argc,
	int *argtv
);

/**
 * Register a component property.
 */
void
entity_object_cproperty_register(
	ComponentType t,
	const char *name,
	Property *prop
);

/**
 * Get the entity associated with given object.
 */
Entity
entity_object_get_entity(Object *o);

/**
 * Initialize a Value with given entity data.
 */
void
entity_object_value_init(Value *v, Entity e);
