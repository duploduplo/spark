#pragma once

#include "common.h"
#include "systems/tilemap.h"

typedef struct PathNode {
	int x;
	int y;
} PathNode;

typedef struct Path Path;

typedef int (*TileCostFunc)(Tile *t, void *data);

Path*
path_new();

void
path_node_add(Path *path, PathNode node);

Path*
path_find(
	Entity map,
	int x1,
	int y1,
	int x2,
	int y2,
	TileCostFunc cost_func,
	void *data
);

size_t
path_len(Path *path);

PathNode*
path_node_get(Path *path, size_t index);

Path*
path_copy(Path *path);

void
path_free(Path *path);
