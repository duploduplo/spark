#pragma once

/**
 * Macro which asserts whether the engine has all of the listed systems
 * registered and prints a stringified system identifier in case of assertion
 * failure.
 */
#define spark_assert_has_systems(...) {\
	int deps[] = {__VA_ARGS__};\
	char **names = g_strsplit(#__VA_ARGS__, ",", 0);\
	for (int i = 0; i < sizeof(deps)/sizeof(int); i++) {\
		System *sys = spark_system_get(deps[i]);\
		if (!sys) {\
			spark_log(\
				LOG_FATAL,\
				"system %s is not registered",\
				g_strstrip(names[i])\
			);\
		}\
	}\
	g_strfreev(names);\
}

/**
 * Bit manipulation utility macros.
 */
#define SET_BIT(var, flag) var |= flag
#define CLR_BIT(var, flag) var &= ~flag
#define HAS_BIT(var, flag) var & flag
