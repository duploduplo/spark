#pragma once

#include "common.h"

enum {
	EVENT_TIMEOUT = 1
};

void
timer_system_register();

void
timer_start(Entity e, int duration);
