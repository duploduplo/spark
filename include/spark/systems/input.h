#pragma once

#include "common.h"

/**
 * Input types.
 */
enum {
	// These have SDL_KeyboardEvent as payload
	EVENT_KEYPRESS     = 1,
	EVENT_KEYRELEASE   = 1 << 1,

	// These have SDL_MouseButtonEvent as payload
	EVENT_MOUSEPRESS   = 1 << 2,
	EVENT_MOUSERELEASE = 1 << 3,
};

/**
 * Register the input system.
 */
void
input_system_register();

/**
 * Initialize the input component.
 *
 * Each input component defines the area of interests, the z index (for input
 * handling and propagation), the input events that it's configured to handle
 * and the forwarding behavior.
 */
void
input_init(Entity e, Rect *area, int z_index, EventType input_type, bool forward);

/**
 * Set the component z-index.
 */
void
input_set_z(Entity e, int z_index);

/**
 * Set the component area of intresets.
 */
void
input_set_area(Entity e, Rect *area);

/**
 * Set the input events that the component will handle.
 */
void
input_set_input_type(Entity e, EventType input_type);

/**
 * Allow/disable forwarding of events.
 */
void
input_set_forward(Entity e, bool forward);

