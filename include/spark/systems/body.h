/******************************************************************************
	Body system (collision detection)
******************************************************************************/

#pragma once

#include "common.h"
#include <stdbool.h>

enum {
	// contains struct Body as payload
	EVENT_COLLISION = 1
};

enum {
	BODY_TYPE_STATIC = 1,
	BODY_TYPE_MOVABLE
};

void
body_system_register();

void
body_init(Entity e, int t, Rect *r, unsigned int group);

void
body_position_set(Entity e, int x, int y);

void
body_position_get(Entity e, int *x, int *y);

Rect*
body_rect(Entity e);

int
body_type(Entity e);

unsigned int
body_group(Entity e);

/*
 * TODO: re-enable these
bool
body_get_nearest(int x, int y, struct Body *dst);

void
body_get_inside_rect(Rect r, struct Body **result_set, size_t *size);
*/
