#pragma once

struct SDL_Surface;

void
image_system_register();

struct SDL_Surface*
image_load(struct System *sys, const char *filename);
