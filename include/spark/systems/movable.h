#pragma once

#include "path.h"

typedef struct MovableUpdateInfo {
	int x, y;
	int node_index;
} MovableUpdateInfo;

/**
 * Events (have MovableUpdateInfo as payload)
 */
enum {
	EVENT_MOVABLE_POSITION_CHANGE = 1,
	EVENT_MOVABLE_NODE_REACH
};

void
movable_system_register();

void
movable_path_set(Entity e, struct Path *p);

void
movable_path_update(Entity e, struct Path *p);

Path*
movable_path_get(Entity e);

void
movable_path_clear(Entity e);

void
movable_speed_set(Entity e, unsigned short speed);

unsigned short
movable_speed_get(Entity e);

void
movable_x_set(Entity entity, int x);

int
movable_x_get(Entity e);

void
movable_y_set(Entity entity, int y);

int
movable_y_get(Entity e);

double
movable_direction_get(Entity e);
